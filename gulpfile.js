'use strict';

// gulp
// gulp default --env=dev
// gulp default --env=stage
// gulp default --env=prod

const { src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const terser = require('gulp-terser');
const browsersync = require('browser-sync').create();
const template = require('gulp-template');
const { readFileSync, writeFileSync, existsSync, mkdirSync } = require("fs");
const rimraf = require("rimraf");
const argv = require('minimist')(process.argv.slice(2));

var command = process.argv[2] || 'default';
var env = (argv.env || argv.environment) || 'dev';

function jsonReader(filePath, cb) {
  readFileSync(filePath, (err, fileData) => {
      if (err) {
          return cb && cb(err)
      }
      try {
          const object = JSON.parse(fileData)
          return cb && cb(null, object)
      } catch(err) {
          return cb && cb(err)
      }
  })
}

function buildEnvironment(envName) {

  var fileEnv = 'src/config/environment.' + envName + '.json';

  var configFilesToReplace = {
    "dest": 'dist/config/environment.json',
    "src": fileEnv
  };

  var srcContent = readFileSync(configFilesToReplace.src, 'utf8');
  
  return writeFileSync(configFilesToReplace.dest, srcContent, 'utf8');
}


function clean(cb) {
  rimraf("dist", function () { });
  cb();
}

function scss() {
  return src('src/scss/**/*.scss', { sourcemaps: true })
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([cssnano()]))
    .pipe(dest('dist/assets/', { sourcemaps: '.' }));
}

function js() {
  return src('src/js/**/*.js', { sourcemaps: true })
    .pipe(terser())
    .pipe(dest('dist/assets/', { sourcemaps: '.' }));
}

function html() {

  var fileEnv = './dist/config/environment.json';

  var srcContent = readFileSync(fileEnv, 'utf8');
  const srcObject = JSON.parse(srcContent);
  console.log(srcObject);
  return src('src/template/**/*.html')
  .pipe(template(srcObject))
  .pipe(dest('dist'));



  readFileSync(fileEnv, (err, fileData) => {
    if (err) {
        return cb && cb(err);
    }
    try {
        const object = JSON.parse(fileData);
        console.log(object);

        return cb && cb(null, object);
    } catch(err) {
        return cb && cb(err);
    }
})

  // jsonReader(fileEnv, (err, fileData) => {
    
  //   if (err) {
  //       console.log(err);
  //       return;
  //   }

  //   console.log(fileData);

  //   return src('src/template/**/*.html')
  //   .pipe(template(fileData))
  //   .pipe(dest('dist'));
  // });

  // try {
  //   var templateOptions = JSON.stringify(srcContent);
  //       templateOptions = JSON.parse(templateOptions);

  //   console.log(templateOptions);
  //   // templateOptions = JSON.parse(JSON.stringify(srcContent));
  //   // console.log(templateOptions);
  //   // console.log(templateOptions.environment.applicationTitle);  
  // } catch (err) {
  //   console.log(err);
  //   return;
  // }
  // return;

  
  // var templateOptions = require('./dist/config/environment.js');
  // console.log(templateOptions.environment);
  // var templateOptions = JSON.parse(srcContent);
  // console.log(templateOptions);
  
}

function staticFiles() {
  return src('src/static/**/*')
    .pipe(dest('dist/static/'));
}

function configFile(cb) {

  var dir = 'dist/config';

  if (!existsSync(dir)) {
    mkdirSync(dir);
    console.log('📁  folder created:', dir);
  }

  buildEnvironment(env);

  cb();
}

function browsersyncServe(cb) {
  browsersync.init({
    server: {
      baseDir: './dist/'
    }
  });
  cb();
}

function browsersyncReload(cb) {
  browsersync.reload();
  cb();
}

function watchFiles() {
  watch('src/template/**/*.html', series(html, browsersyncReload));
  watch(['src/scss/**/*.scss', 'src/js/**/*.js'], series(scss, js, browsersyncReload));
}

exports.default = series(
  scss,
  js,
  configFile,
  html,
  staticFiles,
  browsersyncServe,
  watchFiles
);

exports.clean = series(
  clean
);
