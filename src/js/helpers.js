
// helpers

function isUndefined(value) { return typeof value === 'undefined'; }
function isDefined(value) { return typeof value !== 'undefined'; }
function isObject(value) {
    // http://jsperf.com/isobject4
    return value !== null && typeof value === 'object';
}
function isBlankObject(value) { return value !== null && typeof value === 'object' && !getPrototypeOf(value); }
function isString(value) { return typeof value === 'string'; }
function isNumber(value) { return typeof value === 'number'; }

function toJson(obj, pretty) {
    if (isUndefined(obj)) return undefined;
    if (!isNumber(pretty)) {
        pretty = pretty ? 2 : null;
    }
    return JSON.stringify(obj, toJsonReplacer, pretty);
}

function fromJson(json) {
    return isString(json)
        ? JSON.parse(json)
        : json;
}