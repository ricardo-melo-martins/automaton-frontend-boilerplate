// main.js

function loadJSON(filePath, callback) {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', filePath, true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}

var _cache = window.localStorage;

function init() {
    loadJSON('./config/environment.json', function (response) {
        // Parse JSON string into object
        window.SETTINGS = JSON.parse(response);
        console.log(window.SETTINGS);
        document.getElementById("apiUrl").innerHTML = window.SETTINGS.apiUrl;
    });
}

init();

console.log('finished');