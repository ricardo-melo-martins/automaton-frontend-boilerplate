# Automaton Front End Boilerplate



# Requirements

- Node 12 or later (because dart-sass & gulp-sass v5)
- Gulp-cli v4


## Run

Using `gulp` cli:

```bash
$ gulp
```
or specifying environment for run

```bash
$ gulp default --env=dev
```
or specifying file location:

```bash
$ gulp --gulpfile gulpfile.js --cwd .
```

<br>

Using `npm` cli

<br>
Running on Development Enviroment

```bash
$ npm start
```
or 

```bash
$ npm dev
```
or 

```bash
# script is defined in package.json file
$ npm run gulp
```
<br>

Running on Production Enviroment

```bash
$ npm run build
```
<br>

Running on Staging Enviroment

```bash
$ npm run stage
```

